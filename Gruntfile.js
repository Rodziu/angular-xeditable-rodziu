module.exports = function(grunt){
	var banner = '/*! <%= pkg.description %> v.<%= pkg.version %> | (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %> */\n';
	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		copy: {
			main: {
				files: [
					{expand: true, cwd: 'src/angular-xeditable/src/css', src: '**', dest: 'dist/css'},
					{expand: true, cwd: 'src/js', src: 'angular-xeditable-stubs.js', dest: 'dist/js'}
				]
			}
		},
		concat: {
			dist: {
				options: {
					banner: banner
				},
				src: [
					'src/angular-xeditable/src/js/module.js',
					'src/angular-xeditable/src/js/directives/checkbox.js',
					'src/angular-xeditable/src/js/directives/checklist.js',
					'src/angular-xeditable/src/js/directives/input.js',
					'src/js/directives/radiolist.js',
					'src/angular-xeditable/src/js/directives/select.js',
					'src/angular-xeditable/src/js/directives/textarea.js',
					'src/angular-xeditable/src/js/editable-element/*.js',
					'src/angular-xeditable/src/js/editable-form/*.js',
					'src/js/helpers.js',
					'src/angular-xeditable/src/js/icons.js',
					'src/angular-xeditable/src/js/themes.js',
				],
				dest: 'dist/js/<%= pkg.name %>.js'
			}
		},
		uglify: {
			options: {
				banner: banner
			},
			build: {
				src: '<%= concat.dist.dest %>',
				dest: 'dist/js/<%= pkg.name %>.min.js'
			}
		},
		cssmin: {
			target: {
				options: {
					banner: banner
				},
				files: [{
					expand: true,
					cwd: 'dist/css',
					src: ['*.css', '!*.min.css'],
					dest: 'dist/css',
					ext: '.min.css'
				}]
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');

	grunt.registerTask('default', ['copy', 'concat', 'uglify', 'cssmin']);

};